﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace CRUDA
{
    public partial class AssessCompC : UserControl
    {

        int RubericID;
        int AssessID;
        bool check_date = false;
        string dateC, dataU;
        bool check_M = false;
        bool check_a=false;
        bool check_update = false;
        int id;
        string name;
        int marks;

        public AssessCompC()
        {
            InitializeComponent();        
        }

        private void nametxt_TextChanged(object sender, EventArgs e)
        {
            int i;
            if (markstxt.Text == string.Empty)
            {// check is empty
                lblNameSingal.Text = "Enter the name";
                check_a = false;
            }
            if (markstxt.Text.Any(ch => !char.IsLetter(ch)))
            {//check isSpecialCharactor
               lblNameSingal.Text = "Allowed characters: a-Z";
                check_a = false;
            }
            else { check_M = true; }
        }
        private void set_date()
        {
            dateTimePicker1.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker1.MaxDate = new DateTime(2023, 12, 31);
            dateTimePicker2.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker2.MaxDate = new DateTime(2023, 12, 31);
        }
        private void set_date_on_Click()
        {
            DateTime selectedDateTime1 = dateTimePicker1.Value;
            dateC = selectedDateTime1.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime selectedDateTime2 = dateTimePicker2.Value;
            dataU = selectedDateTime2.ToString("yyyy-MM-dd HH:mm:ss");

            string dateFormat = "yyyy-MM-dd";
            bool validDate1 = DateTime.TryParseExact(dateC, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out selectedDateTime1);
            bool validDate2 = DateTime.TryParseExact(dataU, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out selectedDateTime2);

            if (validDate1 && validDate2)
            {
                if (selectedDateTime2 > selectedDateTime1 || selectedDateTime2 == selectedDateTime1)
                {
                    check_date = true;
                }
            }

        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            set_date_on_Click();
            if (check_date == false&&check_M && check_a)
            {
                if (markstxt.Text != String.Empty && nametxt.Text != String.Empty)
                {
                    Random r = new Random();
                    int x = r.Next(0, 20);
                    var con = Configuration.getInstance().getConnection();

                    SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@name,@RId,@marks,@dateC,@dateU,@AssID)", con);
                    cmd.Parameters.AddWithValue("@marks", markstxt.Text);
                    cmd.Parameters.AddWithValue("@name", nametxt.Text);
                    cmd.Parameters.AddWithValue("@AssID", AssessID.ToString());
                    cmd.Parameters.AddWithValue("@RId", RubericID.ToString());
                    cmd.Parameters.AddWithValue("@dateC", dateC);
                    cmd.Parameters.AddWithValue("@dateU", dateC);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    con.Close();
                    con.Open();
                    markstxt.Text = String.Empty;
                    nametxt.Text = String.Empty;
                }
                else { MessageBox.Show("Fill the data First"); }

            }
            else
            {
                var con2 = Configuration.getInstance().getConnection();
                con2.Open();
                SqlCommand cmd2 = new SqlCommand("Update AssessmentComponent Set Name= @name,RubricId=@RId,TotalMarks=@marks,DateUpdated=@dateU,AssessmentId=@AssID)where Id=@ID", con2);
                cmd2.Parameters.AddWithValue("@marks", markstxt.Text);
                cmd2.Parameters.AddWithValue("@name", nametxt.Text);
                cmd2.Parameters.AddWithValue("@AssID", AssessID.ToString());
                cmd2.Parameters.AddWithValue("@RId", RubericID.ToString());
                cmd2.Parameters.AddWithValue("@dateC", dateC);
                cmd2.Parameters.AddWithValue("@dateU", dataU);
                cmd2.Parameters.AddWithValue("@ID", id);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                con2.Close();
                markstxt.Text = String.Empty;
                nametxt.Text = String.Empty;


            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            markstxt.Text = String.Empty;
            nametxt.Text = String.Empty;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cmbxRuber_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            RubericID = Convert.ToInt32(comboBox2.Items[cmbxRuber.SelectedIndex].ToString()); 
        }

        private void markstxt_TextChanged(object sender, EventArgs e)
        {
            int i;
            if (markstxt.Text == string.Empty)
            {// check is empty
                lblTSignal.Text = "Enter the name";
                check_M = false;
            }
            if (markstxt.Text.Any(ch => !char.IsDigit(ch)))
            {//check isSpecialCharactor
                lblTSignal.Text = "Allowed characters: 1-9";
                check_M = false;
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            AssessID = Convert.ToInt32(comboBox3.Items[comboBox1.SelectedIndex].ToString());
        }
        private void l1()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select  Details FROM Rubric", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbxRuber.Items.Add(reader.GetString(0));
            }
            reader.Close();

            cmd.ExecuteNonQuery();

        }


        private void l3()
        {
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Select  id FROM Rubric", con2);
            SqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
               comboBox2.Items.Add(Convert.ToInt16(reader2.GetInt32(0)));
            }
            reader2.Close();

            cmd2.ExecuteNonQuery();
            
        }
        private void load_combobox_rubric_data()
        {


            cmbxRuber.Items.Clear();
            comboBox2.Items.Clear();
            l1();
            //l2();
            l3();


        }
        private void load_combobox_assessment_data()
        {
            comboBox1.Items.Clear();
            comboBox3.Items.Clear();
            load1();
            load2();
           



        }
        private void load2()
        {
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Select  id FROM Assessment", con2);
            SqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                comboBox3.Items.Add(Convert.ToInt16(reader2.GetInt32(0)));
            }
            reader2.Close();

            cmd2.ExecuteNonQuery();

        }
        private void load1()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select  Title FROM Assessment", con);
            
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                comboBox1.Items.Add(reader.GetString(0));
            }
            reader.Close();

            cmd.ExecuteNonQuery();
        }

        private void AssessCompC_Load(object sender, EventArgs e)
        {
            dateTimePicker1.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker1.MaxDate = new DateTime(2023, 12, 31);
            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Update";
            Update.Text = "Update";
            Update.UseColumnTextForButtonValue = true;

            datagridView.Columns.Add(Update);

            view();
            bool check_update = false;
            view();
           
           comboBox1.Items.Clear();
            comboBox3.Items.Clear();
            comboBox2.Items.Clear();
            load_combobox_assessment_data();
            load_combobox_rubric_data();
            set_date();
        }
        private string check_q(string x )
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand($"        IF(select max(1) from AssessmentComponent where AssessmentId = {AssessID} and AssessmentComponent.Name='{x}' )>0 BEGIN SELECT '1' END ELSE BEGIN SELECT '2' END", con);
            string z = "";
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                z = (reader.GetString(0));
            }
            reader.Close();

            // X=cmd.ExecuteReader().GetString(0);
            cmd.ExecuteNonQuery();
            return z;


        }

        private string check_marks(int z) {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand($"\tdeclare @x as int=(select  sum(AssessmentComponent.TotalMarks) FROM Assessment join AssessmentComponent on AssessmentId=Assessment.Id where AssessmentId={AssessID})\r\n\tdeclare @y as int=(select distinct (Assessment.TotalMarks) FROM Assessment join AssessmentComponent on AssessmentId=Assessment.Id where AssessmentId={AssessID})\r\n\t   IF @x+{z}>@y   BEGIN   SELECT '1' END ELSE BEGIN   SELECT '2' END", con);
            string X = "";
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                X = (reader.GetString(0));
            }
            reader.Close();

            // X=cmd.ExecuteReader().GetString(0);
            cmd.ExecuteNonQuery();
            return X;


        }

        private void btnCreateAccount_Click_1(object sender, EventArgs e)
        {
            set_date_on_Click();
            if (markstxt.Text != String.Empty && nametxt.Text != String.Empty  )
            {
                string y = check_marks(Convert.ToInt32(markstxt.Text));
                string z = check_q(nametxt.Text);
                if (check_date == false && y != "1" )
                {

                    if (check_update == false && z != "1")
                    {

                        if (markstxt.Text != String.Empty && nametxt.Text != String.Empty)
                        {



                            Random r = new Random();
                            int x = r.Next(0, 20);
                            var con = Configuration.getInstance().getConnection();

                            SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@name,@RId,@marks,@dateC,@dateU,@AssID)", con);
                            cmd.Parameters.AddWithValue("@marks", markstxt.Text);
                            cmd.Parameters.AddWithValue("@name", nametxt.Text);
                            cmd.Parameters.AddWithValue("@AssID", AssessID.ToString());
                            cmd.Parameters.AddWithValue("@RId", RubericID.ToString());
                            cmd.Parameters.AddWithValue("@dateC", dateC);
                            cmd.Parameters.AddWithValue("@dateU", dataU);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            con.Close();
                            con.Open();
                            markstxt.Text = String.Empty;
                            nametxt.Text = String.Empty;
                        }
                        else { MessageBox.Show("Fill the data First"); }

                    }
                     else 
                    {
                        label1.Visible = true;
                        dateTimePicker2.Visible = true;
                        var con2 = Configuration.getInstance().getConnection();

                        SqlCommand cmd2 = new SqlCommand($"Update AssessmentComponent Set Name= '{nametxt.Text}',RubricId={RubericID},TotalMarks={markstxt.Text},DateUpdated='{dataU}',AssessmentId={AssessID} where Id={id}", con2);
                        cmd2.Parameters.AddWithValue("@marks", markstxt.Text);
                        cmd2.Parameters.AddWithValue("@name", nametxt.Text);
                        cmd2.Parameters.AddWithValue("@AssID", AssessID.ToString());
                        cmd2.Parameters.AddWithValue("@RId", RubericID.ToString());
                        cmd2.Parameters.AddWithValue("@dateC", dateC);
                        cmd2.Parameters.AddWithValue("@dateU", dataU);
                        cmd2.Parameters.AddWithValue("@ID", id);
                        cmd2.ExecuteNonQuery();
                        MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        markstxt.Text = String.Empty;
                        nametxt.Text = String.Empty;
                        check_update = false;

                    }
                    if (z == "1") { MessageBox.Show("Already Exist"); }
                }
                else { if (y=="1") MessageBox.Show("Componnent Marks greater than total marks of assessment can nit be added"); }
                
            }
            else { MessageBox.Show("Fill the data first"); }

        }
        private void view()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand($"select AssessmentComponent.Id,AssessmentComponent.Name,Rubric.Details,AssessmentComponent.TotalMarks,AssessmentComponent.DateCreated from AssessmentComponent JOIN Rubric ON Rubric.Id = AssessmentComponent.RubricId where AssessmentId = {AssessID}", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;



        }

        private void button1_Click(object sender, EventArgs e)
        {


            view();

        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    nametxt.Text = name;
                    markstxt.Text = marks.ToString();


                    check_update = true;


                }

            }





        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {
            markstxt.Text = String.Empty;
            nametxt.Text = string.Empty;
            
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[1].Value.ToString());
                name = datagridView.Rows[e.RowIndex].Cells[2].Value.ToString();
                marks = Convert.ToInt32(datagridView.Rows[e.RowIndex].Cells[4].Value.ToString());

            }
            catch (Exception exp) { }

        }




    }
}
