﻿using iTextSharp.tool.xml.html.head;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDA
{
    public partial class AssessmentC : UserControl
    {
        bool check_update;
        int id, marks, weightage;
        string title;
        bool check_t = false; bool check_M = false; bool check_W = false;


        public AssessmentC()
        {
            InitializeComponent();
        }
        private String check()
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand($" IF ( select MAX(1) FROM Assessment WHERE Assessment.Title ='{Titletxtbx.Text}') > 0 BEGIN   SELECT '1' END ELSE BEGIN   SELECT '2' END", con);
            string X = "";
           SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                X = (reader.GetString(0));
            }
            reader.Close();

            // X=cmd.ExecuteReader().GetString(0);
            cmd.ExecuteNonQuery();
            return X;


        }
        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            string y=check();
            DateTime selectedDateTime = dateTimePicker1.Value;
            string sqlDateTime = selectedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            if (check_M && check_M && check_W )
            {
                if (check_update == false && y != "1")
                {


                    var con = Configuration.getInstance().getConnection();
                    //con.Open();
                    SqlCommand cmd = new SqlCommand("Insert into Assessment values (@title,@date,@Marks,@Weightage)", con);
                    cmd.Parameters.AddWithValue("@title", (Titletxtbx.Text));
                    cmd.Parameters.AddWithValue("@date", sqlDateTime);
                    cmd.Parameters.AddWithValue("@Marks", markstxtbx.Text);
                    cmd.Parameters.AddWithValue("@Weightage", Weightagetxtbx.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // con.Close();
                    Titletxtbx.Text = String.Empty;
                    markstxtbx.Text = String.Empty;
                    Weightagetxtbx.Text = String.Empty;
                    MessageBox.Show("ADD Assessment Components ");
                    AssessCompC newUserControl = new AssessCompC();
                    newUserControl.Dock = DockStyle.Fill;
                    this.Parent.Controls.Add(newUserControl);
                    newUserControl.BringToFront();
                    this.Hide();



                }
                else if (check_update == true)
                {

                    var con2 = Configuration.getInstance().getConnection();
                    //con2.Open();
                    SqlCommand cmd2 = new SqlCommand("Update Assessment Set Title = @title, DateCreated = @date, TotalMarks = @Marks, TotalWeightage = @Weightage  WHERE Id = @ID", con2);
                    cmd2.Parameters.AddWithValue("@title", (Titletxtbx.Text));
                    cmd2.Parameters.AddWithValue("@date", sqlDateTime);
                    cmd2.Parameters.AddWithValue("@Marks", markstxtbx.Text);
                    cmd2.Parameters.AddWithValue("@Weightage", Weightagetxtbx.Text);
                    cmd2.Parameters.AddWithValue("@ID", id);
                    cmd2.ExecuteNonQuery();
                    MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    con2.Close();
                    con2.Open();
                    check_update = false;
                    Titletxtbx.Text = String.Empty;
                    markstxtbx.Text = String.Empty;
                    Weightagetxtbx.Text = String.Empty;
                }
                else
                {

                    if (y == "1") { MessageBox.Show("Already Exist"); }
                }


            }
            else {

            }
            


            // Form ass = new Assessment_Component_Form();
            // ass.Show();
            // this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Titletxtbx.Text = String.Empty;
            markstxtbx.Text = String.Empty;
            Weightagetxtbx.Text = String.Empty;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void view()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from Assessment", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;



        }
        private void button1_Click(object sender, EventArgs e)
        {
            view();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Titletxtbx_TextChanged(object sender, EventArgs e)
        {
            int i;
            if (Titletxtbx.Text == string.Empty)
            {// check is empty
                lblTitalSingal.Text = "Enter the name";
                check_t = false;
            }
            //else if (int.TryParse(Firsttxtbx.Text, out i))
            //{//Check isnumberic
            //    lblFirstNameSingal.Text = "Allowed characters: a-z, A-Z";
            //    check_f = false;
            //}
            else if (Titletxtbx.Text.Any(ch => !char.IsLetterOrDigit(ch) ))

            {//check isSpecialCharactor
                lblTitalSingal.Text = "Allowed characters: a-z, A-Z";
                check_t = false;
            }
            else
            {//ready for storage or action
                lblTitalSingal.Text = " ";
                check_t = true;
            }
        }

        private void markstxtbx_TextChanged(object sender, EventArgs e)
        {
            //markstxtbx.Text = string.Empty;
            int i;
            if (markstxtbx.Text == string.Empty)
            {// check is empty
                labelmarkssingnal.Text = "Enter the marks";
                check_M = false;
            }
            if (markstxtbx.Text.Any(ch => !char.IsDigit(ch)))
            {//check isSpecialCharactor
                labelmarkssingnal.Text = "Allowed characters: 1-9";
                check_M = false;
            }

            else



            {//ready for storage or action
                labelmarkssingnal.Text = " ";
                check_M = true;
            }


        }

        private void Weightagetxtbx_TextChanged(object sender, EventArgs e)
        {
            int i;
            if (Weightagetxtbx.Text == string.Empty)
            {// check is empty
                lblWsignla.Text = "Enter the marks";
                check_W = false;
            }
            if (Weightagetxtbx.Text.Any(ch => !char.IsDigit(ch)))
            {//check isSpecialCharactor
                lblWsignla.Text = "Allowed characters: 1-9";
                check_W = false;
            }

            else

            {//ready for storage or action
                lblWsignla.Text = " ";
                check_W = true;
            }

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            AssessCompC newUserControl = new AssessCompC();
            newUserControl.Dock = DockStyle.Fill;
            this.Parent.Controls.Add(newUserControl);
            newUserControl.BringToFront();
            this.Hide();

        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[1].Value.ToString());
                title = datagridView.Rows[e.RowIndex].Cells[2].Value.ToString();
                marks = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[4].Value.ToString());
                weightage = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[5].Value.ToString());

            }
            catch (Exception exp) { }
        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    Titletxtbx.Text = title;
                    markstxtbx.Text = marks.ToString();
                    Weightagetxtbx.Text = weightage.ToString();

                    check_update = true;
                    dateTimePicker1.Visible = false;
                    label3.Visible = false;

                }

            }

        }

        private void AssessmentC_Load(object sender, EventArgs e)
        {


                dateTimePicker1.MinDate = new DateTime(2023, 1, 1);
                dateTimePicker1.MaxDate = new DateTime(2023, 12, 31);
                DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
                Update.HeaderText = "Update";
                Update.Text = "Update";
                Update.UseColumnTextForButtonValue = true;

                datagridView.Columns.Add(Update);

                view();
                bool check_update = false;
            
        }
    }
}
