﻿namespace CRUDA
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbxStatus = new System.Windows.Forms.ComboBox();
            this.lblRecordSignal = new System.Windows.Forms.Label();
            this.lblEmailAddressSignal = new System.Windows.Forms.Label();
            this.lblResgSignal = new System.Windows.Forms.Label();
            this.lbllastNameSignal = new System.Windows.Forms.Label();
            this.lblFirstNameSingal = new System.Windows.Forms.Label();
            this.btnCreateAccount = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtbxEmailAddress = new System.Windows.Forms.TextBox();
            this.txtbxRrgisteration = new System.Windows.Forms.TextBox();
            this.txtLASTName = new System.Windows.Forms.TextBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblSignUp = new System.Windows.Forms.Label();
            this.gbx = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblContactNumberSignal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbxContactNumber = new System.Windows.Forms.TextBox();
            this.Lblontact = new System.Windows.Forms.Label();
            this.Firsttxtbx = new System.Windows.Forms.TextBox();
            this.LastNamelbl = new System.Windows.Forms.Label();
            this.Registerationlbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.gbx.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbxStatus
            // 
            this.cmbxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbxStatus.FormattingEnabled = true;
            this.cmbxStatus.Items.AddRange(new object[] {
            "Active",
            "InActive"});
            this.cmbxStatus.Location = new System.Drawing.Point(545, 200);
            this.cmbxStatus.Name = "cmbxStatus";
            this.cmbxStatus.Size = new System.Drawing.Size(173, 21);
            this.cmbxStatus.Sorted = true;
            this.cmbxStatus.TabIndex = 34;
            this.cmbxStatus.SelectedIndexChanged += new System.EventHandler(this.cmbxStatus_SelectedIndexChanged);
            // 
            // lblRecordSignal
            // 
            this.lblRecordSignal.AutoSize = true;
            this.lblRecordSignal.Location = new System.Drawing.Point(568, 463);
            this.lblRecordSignal.Name = "lblRecordSignal";
            this.lblRecordSignal.Size = new System.Drawing.Size(10, 13);
            this.lblRecordSignal.TabIndex = 33;
            this.lblRecordSignal.Text = " ";
            this.lblRecordSignal.Click += new System.EventHandler(this.lblRecordSignal_Click);
            // 
            // lblEmailAddressSignal
            // 
            this.lblEmailAddressSignal.AccessibleName = "lblEmailSignal";
            this.lblEmailAddressSignal.AutoSize = true;
            this.lblEmailAddressSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblEmailAddressSignal.Location = new System.Drawing.Point(545, 156);
            this.lblEmailAddressSignal.Name = "lblEmailAddressSignal";
            this.lblEmailAddressSignal.Size = new System.Drawing.Size(10, 13);
            this.lblEmailAddressSignal.TabIndex = 25;
            this.lblEmailAddressSignal.Text = " ";
            this.lblEmailAddressSignal.Click += new System.EventHandler(this.lblEmailAddressSignal_Click);
            // 
            // lblResgSignal
            // 
            this.lblResgSignal.AccessibleName = "lblResgSignal";
            this.lblResgSignal.AutoSize = true;
            this.lblResgSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblResgSignal.Location = new System.Drawing.Point(210, 237);
            this.lblResgSignal.Name = "lblResgSignal";
            this.lblResgSignal.Size = new System.Drawing.Size(10, 13);
            this.lblResgSignal.TabIndex = 20;
            this.lblResgSignal.Text = " ";
            this.lblResgSignal.Click += new System.EventHandler(this.lblUsernameSignal_Click);
            // 
            // lbllastNameSignal
            // 
            this.lbllastNameSignal.AutoSize = true;
            this.lbllastNameSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lbllastNameSignal.Location = new System.Drawing.Point(210, 156);
            this.lbllastNameSignal.Name = "lbllastNameSignal";
            this.lbllastNameSignal.Size = new System.Drawing.Size(10, 13);
            this.lbllastNameSignal.TabIndex = 19;
            this.lbllastNameSignal.Text = " ";
            this.lbllastNameSignal.Click += new System.EventHandler(this.lbllastNameSignal_Click);
            // 
            // lblFirstNameSingal
            // 
            this.lblFirstNameSingal.AutoSize = true;
            this.lblFirstNameSingal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblFirstNameSingal.Location = new System.Drawing.Point(210, 99);
            this.lblFirstNameSingal.Name = "lblFirstNameSingal";
            this.lblFirstNameSingal.Size = new System.Drawing.Size(10, 13);
            this.lblFirstNameSingal.TabIndex = 1;
            this.lblFirstNameSingal.Text = " ";
            this.lblFirstNameSingal.Click += new System.EventHandler(this.lblFirstNameSingal_Click);
            // 
            // btnCreateAccount
            // 
            this.btnCreateAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(100)))), ((int)(((byte)(26)))));
            this.btnCreateAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCreateAccount.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCreateAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateAccount.ForeColor = System.Drawing.Color.White;
            this.btnCreateAccount.Location = new System.Drawing.Point(555, 3);
            this.btnCreateAccount.Name = "btnCreateAccount";
            this.btnCreateAccount.Size = new System.Drawing.Size(185, 29);
            this.btnCreateAccount.TabIndex = 13;
            this.btnCreateAccount.Text = "Create Account";
            this.btnCreateAccount.UseVisualStyleBackColor = false;
            this.btnCreateAccount.Click += new System.EventHandler(this.btnCreateAccount_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(97)))), ((int)(((byte)(139)))));
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClear.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(336, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(213, 29);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear All";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(146, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(184, 29);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtbxEmailAddress
            // 
            this.txtbxEmailAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbxEmailAddress.Location = new System.Drawing.Point(545, 128);
            this.txtbxEmailAddress.Name = "txtbxEmailAddress";
            this.txtbxEmailAddress.Size = new System.Drawing.Size(173, 20);
            this.txtbxEmailAddress.TabIndex = 9;
            this.txtbxEmailAddress.TextChanged += new System.EventHandler(this.txtbxEmailAddress_TextChanged);
            // 
            // txtbxRrgisteration
            // 
            this.txtbxRrgisteration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbxRrgisteration.Location = new System.Drawing.Point(210, 200);
            this.txtbxRrgisteration.Name = "txtbxRrgisteration";
            this.txtbxRrgisteration.Size = new System.Drawing.Size(179, 20);
            this.txtbxRrgisteration.TabIndex = 2;
            this.txtbxRrgisteration.TextChanged += new System.EventHandler(this.txtbxRrgisteration_TextChanged);
            // 
            // txtLASTName
            // 
            this.txtLASTName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLASTName.Location = new System.Drawing.Point(210, 128);
            this.txtLASTName.Name = "txtLASTName";
            this.txtLASTName.Size = new System.Drawing.Size(179, 20);
            this.txtLASTName.TabIndex = 1;
            this.txtLASTName.TextChanged += new System.EventHandler(this.txtLASTName_TextChanged);
            // 
            // lblFirstName
            // 
            this.lblFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.lblFirstName.Location = new System.Drawing.Point(90, 61);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(86, 38);
            this.lblFirstName.TabIndex = 2;
            this.lblFirstName.Text = "First Name";
            this.lblFirstName.Click += new System.EventHandler(this.lblFirstName_Click);
            // 
            // lblSignUp
            // 
            this.lblSignUp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSignUp.AutoSize = true;
            this.lblSignUp.Font = new System.Drawing.Font("Castellar", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignUp.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSignUp.Location = new System.Drawing.Point(347, 8);
            this.lblSignUp.Name = "lblSignUp";
            this.lblSignUp.Size = new System.Drawing.Size(207, 25);
            this.lblSignUp.TabIndex = 0;
            this.lblSignUp.Text = "Student Form";
            this.lblSignUp.Click += new System.EventHandler(this.lblSignUp_Click);
            // 
            // gbx
            // 
            this.gbx.Controls.Add(this.tableLayoutPanel1);
            this.gbx.Controls.Add(this.lblRecordSignal);
            this.gbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbx.Location = new System.Drawing.Point(0, 0);
            this.gbx.Name = "gbx";
            this.gbx.Size = new System.Drawing.Size(914, 501);
            this.gbx.TabIndex = 1;
            this.gbx.TabStop = false;
            this.gbx.Enter += new System.EventHandler(this.gbx_Enter);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.80353F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.19646F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(908, 482);
            this.tableLayoutPanel1.TabIndex = 42;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint_1);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lblSignUp, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(902, 41);
            this.tableLayoutPanel2.TabIndex = 0;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.44828F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.55173F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel3.Controls.Add(this.cmbxStatus, 4, 5);
            this.tableLayoutPanel3.Controls.Add(this.lblEmailAddressSignal, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.label1, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.lblContactNumberSignal, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.label2, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtbxEmailAddress, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtbxContactNumber, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.Lblontact, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.Firsttxtbx, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblFirstName, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblFirstNameSingal, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtLASTName, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.LastNamelbl, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lbllastNameSignal, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtbxRrgisteration, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.Registerationlbl, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.lblResgSignal, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.button1, 4, 6);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 50);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 8;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.73913F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.26087F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(902, 319);
            this.tableLayoutPanel3.TabIndex = 1;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.label1.Location = new System.Drawing.Point(441, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 40);
            this.label1.TabIndex = 41;
            this.label1.Text = "Status";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblContactNumberSignal
            // 
            this.lblContactNumberSignal.AccessibleName = "lblContactSignal";
            this.lblContactNumberSignal.AutoSize = true;
            this.lblContactNumberSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblContactNumberSignal.Location = new System.Drawing.Point(545, 99);
            this.lblContactNumberSignal.Name = "lblContactNumberSignal";
            this.lblContactNumberSignal.Size = new System.Drawing.Size(10, 13);
            this.lblContactNumberSignal.TabIndex = 24;
            this.lblContactNumberSignal.Text = " ";
            this.lblContactNumberSignal.Click += new System.EventHandler(this.lblContactNumberSignal_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.label2.Location = new System.Drawing.Point(441, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 31);
            this.label2.TabIndex = 38;
            this.label2.Text = "Email ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtbxContactNumber
            // 
            this.txtbxContactNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbxContactNumber.Location = new System.Drawing.Point(545, 64);
            this.txtbxContactNumber.Name = "txtbxContactNumber";
            this.txtbxContactNumber.Size = new System.Drawing.Size(173, 20);
            this.txtbxContactNumber.TabIndex = 8;
            this.txtbxContactNumber.TextChanged += new System.EventHandler(this.txtbxContactNumber_TextChanged);
            // 
            // Lblontact
            // 
            this.Lblontact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lblontact.AutoSize = true;
            this.Lblontact.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.Lblontact.Location = new System.Drawing.Point(414, 61);
            this.Lblontact.Name = "Lblontact";
            this.Lblontact.Size = new System.Drawing.Size(125, 21);
            this.Lblontact.TabIndex = 39;
            this.Lblontact.Text = "Contact Number";
            this.Lblontact.Click += new System.EventHandler(this.Lblontact_Click);
            // 
            // Firsttxtbx
            // 
            this.Firsttxtbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Firsttxtbx.Location = new System.Drawing.Point(210, 64);
            this.Firsttxtbx.Name = "Firsttxtbx";
            this.Firsttxtbx.Size = new System.Drawing.Size(179, 20);
            this.Firsttxtbx.TabIndex = 40;
            this.Firsttxtbx.TextChanged += new System.EventHandler(this.Firsttxtbx_TextChanged);
            // 
            // LastNamelbl
            // 
            this.LastNamelbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LastNamelbl.AutoSize = true;
            this.LastNamelbl.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.LastNamelbl.Location = new System.Drawing.Point(91, 125);
            this.LastNamelbl.Name = "LastNamelbl";
            this.LastNamelbl.Size = new System.Drawing.Size(84, 31);
            this.LastNamelbl.TabIndex = 36;
            this.LastNamelbl.Text = "Last Name";
            this.LastNamelbl.Click += new System.EventHandler(this.LastNamelbl_Click);
            // 
            // Registerationlbl
            // 
            this.Registerationlbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Registerationlbl.AutoSize = true;
            this.Registerationlbl.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Registerationlbl.Location = new System.Drawing.Point(72, 197);
            this.Registerationlbl.Name = "Registerationlbl";
            this.Registerationlbl.Size = new System.Drawing.Size(121, 40);
            this.Registerationlbl.TabIndex = 37;
            this.Registerationlbl.Text = "Registeration ID";
            this.Registerationlbl.Click += new System.EventHandler(this.Registerationlbl_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(97)))), ((int)(((byte)(139)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(545, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(173, 29);
            this.button1.TabIndex = 42;
            this.button1.Text = "View Students\r\n";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.34234F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.65766F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158F));
            this.tableLayoutPanel4.Controls.Add(this.btnClose, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCreateAccount, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnClear, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 375);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(902, 104);
            this.tableLayoutPanel4.TabIndex = 2;
            this.tableLayoutPanel4.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel4_Paint);
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(914, 501);
            this.Controls.Add(this.gbx);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StudentForm";
            this.Text = "StudentForm";
            this.Load += new System.EventHandler(this.StudentForm_Load);
            this.gbx.ResumeLayout(false);
            this.gbx.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbxStatus;
        private System.Windows.Forms.Label lblRecordSignal;
        private System.Windows.Forms.Label lblEmailAddressSignal;
        private System.Windows.Forms.Label lblResgSignal;
        private System.Windows.Forms.Label lbllastNameSignal;
        private System.Windows.Forms.Label lblFirstNameSingal;
        private System.Windows.Forms.Button btnCreateAccount;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtbxEmailAddress;
        private System.Windows.Forms.TextBox txtbxRrgisteration;
        private System.Windows.Forms.TextBox txtLASTName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblSignUp;
        private System.Windows.Forms.GroupBox gbx;
        private System.Windows.Forms.Label LastNamelbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Registerationlbl;
        private System.Windows.Forms.Label lblContactNumberSignal;
        private System.Windows.Forms.TextBox txtbxContactNumber;
        private System.Windows.Forms.Label Lblontact;
        private System.Windows.Forms.TextBox Firsttxtbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button button1;
    }
}