﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Runtime.CompilerServices;


namespace CRUDA
{
    public partial class StudentListForm : Form
    {
        string first, last, registeration, email, contact ;
        int  id, status;

        public StudentListForm()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {


        }
        private void DataBind()
        {


            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Update";
            Update.Text = "Update";
            Update.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn Delete = new DataGridViewButtonColumn();
            Delete.HeaderText = "Delete";
            Delete.Text = "Delete";
            Delete.UseColumnTextForButtonValue = true;
            datagridView.Columns.Add(Update);
            datagridView.Columns.Add(Delete);
        }


        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    Form form = new StudentForm( id, first, last, registeration, email,  contact, status,  true);
                    form.ShowDialog();

                    
                }
                else if (index == 1)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand($"Delete FROM Student  where Id= {id}", con);
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Deleted Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    var con2 = Configuration.getInstance().getConnection();
                    con2.Open();
                    SqlCommand command2 = new SqlCommand("Select count(*) from Student where status=6", con2);
                    int count2 = (int)command2.ExecuteScalar();
                    Inactivecount.Text = count2.ToString();
                    con2.Close();

                    var con3 = Configuration.getInstance().getConnection();
                    con3.Open();
                    SqlCommand command3 = new SqlCommand("Select count(*) from Student", con3);
                    int count3 = (int)command3.ExecuteScalar();
                    totalcount.Text = count3.ToString();
                    con3.Close();


                    var con4 = Configuration.getInstance().getConnection();
                    con4.Open();
                    SqlCommand command = new SqlCommand("Select count(*) from Student where status=5", con4);
                    int count = (int)command.ExecuteScalar();
                    activecount.Text = count.ToString();
                    con.Close();



                }
            }
            

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void StudentListForm_Load(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor=Color.Black;
            DataBind();
            totalcount.ForeColor = Color.Navy;


            var con2 = Configuration.getInstance().getConnection();
         //  con2.Open();
            SqlCommand command2 = new SqlCommand("Select count(*) from Student where status=6", con2);
            int count2 = (int)command2.ExecuteScalar();
            Inactivecount.Text = count2.ToString();
            con2.Close();
            
            var con3 = Configuration.getInstance().getConnection();
            con3.Open();
            SqlCommand command3 = new SqlCommand("Select count(*) from Student", con3);
            int count3 = (int)command3.ExecuteScalar();
            totalcount.Text = count3.ToString();
            con3.Close();


            var con4 = Configuration.getInstance().getConnection();
            con4.Open();
            SqlCommand command = new SqlCommand("Select count(*) from Student where status=5", con4);
            int count = (int)command.ExecuteScalar();
            activecount.Text = count.ToString();
            con.Close();
            con2.Open();    


        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gbx_Enter(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblSignUp_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void ExportToPDF(DataGridView dgv)
        {

            Document document = new Document(PageSize.A4, 20, 20, 20, 20);
            PdfWriter.GetInstance(document, new FileStream("Student Reports.pdf", FileMode.Create));
            document.AddHeader("Header", "Report of Student list");
            document.AddHeader("Date", DateTime.Now.ToString());
            document.Open();
            // Create a table with the same number of columns as the DataGridView
            PdfPTable table = new PdfPTable(dgv.Columns.Count);
            // Add the column headers from the DataGridView to the table
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                table.AddCell(cell);
            }

            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Index == datagridView.Rows.Count - 1)
                {
                    continue;

                }
                else
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString()));
                        table.AddCell(pdfCell);
                    }

                }


            }
            document.Add(table);
            document.Close();

            // Close the document
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportToPDF(datagridView);        
        }

        private void tableLayoutPanel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Inactivecount_Click(object sender, EventArgs e)
        {

        }

        private void totalcount_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[2].Value.ToString());
                first = datagridView.Rows[e.RowIndex].Cells[3].Value.ToString();
                last = datagridView.Rows[e.RowIndex].Cells[4].Value.ToString();
                contact = datagridView.Rows[e.RowIndex].Cells[5].Value.ToString();
                email = datagridView.Rows[e.RowIndex].Cells[6].Value.ToString();
                registeration = datagridView.Rows[e.RowIndex].Cells[7].Value.ToString();
                status = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[8].Value.ToString());
            }
            catch (Exception exp) { }




        }

        private void activecount_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {




        }
    }
}
    