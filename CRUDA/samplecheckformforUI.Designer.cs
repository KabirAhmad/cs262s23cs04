﻿namespace CRUDA
{
    partial class samplecheckformforUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFirstNameSingal = new System.Windows.Forms.Label();
            this.gbx = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSignUp = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblEmailAddressSignal = new System.Windows.Forms.Label();
            this.lblContactNumberSignal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblSID = new System.Windows.Forms.Label();
            this.lbllastNameSignal = new System.Windows.Forms.Label();
            this.lblResgSignal = new System.Windows.Forms.Label();
            this.LblAID = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cmbxRuber = new System.Windows.Forms.ComboBox();
            this.SIDcmbx = new System.Windows.Forms.ComboBox();
            this.Registerationlbl = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCreateAccount = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblRecordSignal = new System.Windows.Forms.Label();
            this.gbx.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFirstNameSingal
            // 
            this.lblFirstNameSingal.AutoSize = true;
            this.lblFirstNameSingal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblFirstNameSingal.Location = new System.Drawing.Point(177, 46);
            this.lblFirstNameSingal.Name = "lblFirstNameSingal";
            this.lblFirstNameSingal.Size = new System.Drawing.Size(10, 13);
            this.lblFirstNameSingal.TabIndex = 1;
            this.lblFirstNameSingal.Text = " ";
            // 
            // gbx
            // 
            this.gbx.Controls.Add(this.tableLayoutPanel1);
            this.gbx.Controls.Add(this.lblRecordSignal);
            this.gbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbx.Location = new System.Drawing.Point(0, 0);
            this.gbx.Name = "gbx";
            this.gbx.Size = new System.Drawing.Size(800, 450);
            this.gbx.TabIndex = 4;
            this.gbx.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.80353F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.19646F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(794, 431);
            this.tableLayoutPanel1.TabIndex = 42;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lblSignUp, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(788, 41);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lblSignUp
            // 
            this.lblSignUp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSignUp.AutoSize = true;
            this.lblSignUp.Font = new System.Drawing.Font("Castellar", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignUp.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSignUp.Location = new System.Drawing.Point(281, 8);
            this.lblSignUp.Name = "lblSignUp";
            this.lblSignUp.Size = new System.Drawing.Size(225, 25);
            this.lblSignUp.TabIndex = 0;
            this.lblSignUp.Text = "Student Result";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.10101F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.89899F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 142F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel3.Controls.Add(this.lblEmailAddressSignal, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblContactNumberSignal, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.label2, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblSID, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblFirstNameSingal, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.lbllastNameSignal, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblResgSignal, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.LblAID, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.dateTimePicker2, 4, 5);
            this.tableLayoutPanel3.Controls.Add(this.comboBox1, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmbxRuber, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.SIDcmbx, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.Registerationlbl, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.comboBox2, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.label1, 1, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 50);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 8;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.86364F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.13636F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(788, 308);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // lblEmailAddressSignal
            // 
            this.lblEmailAddressSignal.AccessibleName = "lblEmailSignal";
            this.lblEmailAddressSignal.AutoSize = true;
            this.lblEmailAddressSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblEmailAddressSignal.Location = new System.Drawing.Point(562, 145);
            this.lblEmailAddressSignal.Name = "lblEmailAddressSignal";
            this.lblEmailAddressSignal.Size = new System.Drawing.Size(10, 13);
            this.lblEmailAddressSignal.TabIndex = 25;
            this.lblEmailAddressSignal.Text = " ";
            // 
            // lblContactNumberSignal
            // 
            this.lblContactNumberSignal.AccessibleName = "lblContactSignal";
            this.lblContactNumberSignal.AutoSize = true;
            this.lblContactNumberSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblContactNumberSignal.Location = new System.Drawing.Point(562, 46);
            this.lblContactNumberSignal.Name = "lblContactNumberSignal";
            this.lblContactNumberSignal.Size = new System.Drawing.Size(10, 13);
            this.lblContactNumberSignal.TabIndex = 24;
            this.lblContactNumberSignal.Text = " ";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.label2.Location = new System.Drawing.Point(349, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 73);
            this.label2.TabIndex = 38;
            this.label2.Text = "Assessment Component";
            // 
            // lblSID
            // 
            this.lblSID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblSID.AutoSize = true;
            this.lblSID.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.lblSID.Location = new System.Drawing.Point(54, 11);
            this.lblSID.Name = "lblSID";
            this.lblSID.Size = new System.Drawing.Size(82, 35);
            this.lblSID.TabIndex = 2;
            this.lblSID.Text = "Student ID";
            // 
            // lbllastNameSignal
            // 
            this.lbllastNameSignal.AutoSize = true;
            this.lbllastNameSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lbllastNameSignal.Location = new System.Drawing.Point(177, 145);
            this.lbllastNameSignal.Name = "lbllastNameSignal";
            this.lbllastNameSignal.Size = new System.Drawing.Size(10, 13);
            this.lbllastNameSignal.TabIndex = 19;
            this.lbllastNameSignal.Text = " ";
            // 
            // lblResgSignal
            // 
            this.lblResgSignal.AccessibleName = "lblResgSignal";
            this.lblResgSignal.AutoSize = true;
            this.lblResgSignal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.lblResgSignal.Location = new System.Drawing.Point(177, 226);
            this.lblResgSignal.Name = "lblResgSignal";
            this.lblResgSignal.Size = new System.Drawing.Size(10, 13);
            this.lblResgSignal.TabIndex = 20;
            this.lblResgSignal.Text = " ";
            // 
            // LblAID
            // 
            this.LblAID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LblAID.AutoSize = true;
            this.LblAID.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.LblAID.Location = new System.Drawing.Point(382, 11);
            this.LblAID.Name = "LblAID";
            this.LblAID.Size = new System.Drawing.Size(111, 35);
            this.LblAID.TabIndex = 39;
            this.LblAID.Text = "Assessment ID";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(562, 170);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(136, 20);
            this.dateTimePicker2.TabIndex = 43;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(562, 14);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(136, 21);
            this.comboBox1.TabIndex = 44;
            // 
            // cmbxRuber
            // 
            this.cmbxRuber.FormattingEnabled = true;
            this.cmbxRuber.Location = new System.Drawing.Point(562, 75);
            this.cmbxRuber.Name = "cmbxRuber";
            this.cmbxRuber.Size = new System.Drawing.Size(136, 21);
            this.cmbxRuber.TabIndex = 45;
            // 
            // SIDcmbx
            // 
            this.SIDcmbx.Dock = System.Windows.Forms.DockStyle.Left;
            this.SIDcmbx.FormattingEnabled = true;
            this.SIDcmbx.Location = new System.Drawing.Point(177, 14);
            this.SIDcmbx.Name = "SIDcmbx";
            this.SIDcmbx.Size = new System.Drawing.Size(137, 21);
            this.SIDcmbx.TabIndex = 46;
            // 
            // Registerationlbl
            // 
            this.Registerationlbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Registerationlbl.AutoSize = true;
            this.Registerationlbl.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Registerationlbl.Location = new System.Drawing.Point(26, 72);
            this.Registerationlbl.Name = "Registerationlbl";
            this.Registerationlbl.Size = new System.Drawing.Size(138, 73);
            this.Registerationlbl.TabIndex = 37;
            this.Registerationlbl.Text = "Measurment Level";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(177, 75);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(137, 21);
            this.comboBox2.TabIndex = 47;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.label1.Location = new System.Drawing.Point(27, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 59);
            this.label1.TabIndex = 41;
            this.label1.Text = "Date of Evaluation";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.34234F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.65766F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159F));
            this.tableLayoutPanel4.Controls.Add(this.btnClose, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCreateAccount, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnClear, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 376);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62.31884F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.68116F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(788, 52);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(95, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 26);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // btnCreateAccount
            // 
            this.btnCreateAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(100)))), ((int)(((byte)(26)))));
            this.btnCreateAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCreateAccount.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCreateAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateAccount.ForeColor = System.Drawing.Color.White;
            this.btnCreateAccount.Location = new System.Drawing.Point(440, 3);
            this.btnCreateAccount.Name = "btnCreateAccount";
            this.btnCreateAccount.Size = new System.Drawing.Size(185, 26);
            this.btnCreateAccount.TabIndex = 13;
            this.btnCreateAccount.Text = "Create Account";
            this.btnCreateAccount.UseVisualStyleBackColor = false;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(97)))), ((int)(((byte)(139)))));
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClear.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(221, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(213, 26);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear All";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // lblRecordSignal
            // 
            this.lblRecordSignal.AutoSize = true;
            this.lblRecordSignal.Location = new System.Drawing.Point(568, 463);
            this.lblRecordSignal.Name = "lblRecordSignal";
            this.lblRecordSignal.Size = new System.Drawing.Size(10, 13);
            this.lblRecordSignal.TabIndex = 33;
            this.lblRecordSignal.Text = " ";
            // 
            // samplecheckformforUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gbx);
            this.Name = "samplecheckformforUI";
            this.Text = "samplecheckformforUI";
            this.gbx.ResumeLayout(false);
            this.gbx.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFirstNameSingal;
        private System.Windows.Forms.GroupBox gbx;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblSignUp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblEmailAddressSignal;
        private System.Windows.Forms.Label lblContactNumberSignal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblSID;
        private System.Windows.Forms.Label lbllastNameSignal;
        private System.Windows.Forms.Label lblResgSignal;
        private System.Windows.Forms.Label LblAID;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox cmbxRuber;
        private System.Windows.Forms.ComboBox SIDcmbx;
        private System.Windows.Forms.Label Registerationlbl;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCreateAccount;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblRecordSignal;
    }
}