﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDA
{
    public partial class DashBoard : Form
    {
        bool evaluation;
        string e_name;
        int e_id;


        bool studentlist;


        public DashBoard()
        {
            InitializeComponent();

        }
        public DashBoard(bool evaluation, string e_name, int e_id)
        {
            InitializeComponent();
            this.evaluation = evaluation;
            this.e_name = e_name;
            this.e_id = e_id;

        }
        public DashBoard(bool studentList)
        {
            InitializeComponent();
            this.studentlist = studentList;


        }


        private void pParent_Paint(object sender, PaintEventArgs e)
        {

        }
        public void loadform(object form)
        {

            Form f = form as Form;
            f.TopLevel = false;
            f.Dock = DockStyle.Fill;
            this.pParent.Controls.Add(f);
            this.pParent.Tag = f;
            f.Show();

        }
        public void loadc(object usercontrol)
        {

            UserControl f = usercontrol as UserControl;
            //f.TopLevel = false;
            f.Dock = DockStyle.Fill;
            this.pParent.Controls.Add(f);
            this.pParent.Tag = f;
            f.Show();

        }

        private void Studentfromlbl_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new Student());

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStriplblDate.Text = DateTime.Now.ToString("dddd dd/MM/yyyy");
            toolStriplblTime.Text = DateTime.Now.ToString("hh:mm:ss:tt");
            if (evaluation == true)
            {

                loadform(new Evaluation_Form(e_name, e_id));
                evaluation = false;
            }
            else if (studentlist == true)
            {
                loadform(new StudentListForm());
                studentlist = false;
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Attendance_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadform(new ClassAttendence());

        }

        private void Assessmentlbl_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new AssessmentC());
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadform(new CLOsForm());
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new RubricC());
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadform(new Ruberic_Level_Form());
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void studentListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new StudentListControl());
        }

        private void rubricsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new RubricLevelC());
        }

        // public void openSL() { loadform(new StudentListForm()); }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }// In Form1


        // In Form2

        public void closef() { this.Close(); }

        private void label1_Click_1(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new StudentRC());
        }

        private void label4_Click_1(object sender, EventArgs e)
        {
            this.pParent.Controls.Clear();
            loadc(new ReportsC());
        }

        private void label5_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void rubericLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                this.pParent.Controls.Clear();
                loadc(new AssessCompC());
            
        }

        private void assessmentComponentsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.pParent.Controls.Clear();
            loadc(new ViewAttendanceC());
        }
    }
}
