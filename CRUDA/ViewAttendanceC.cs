﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf.draw;
using System.ComponentModel.Design.Serialization;
using System.Globalization;

namespace CRUDA
{
    public partial class ViewAttendanceC : UserControl
    {
        string x;

        public ViewAttendanceC()
        {
            InitializeComponent();
        }

        private void ViewAttendanceC_Load(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            load_student_attendance();
            loads_box();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void loads_box()
        {
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("\r\nselect Distinct(AttendanceDate) from ClassAttendance join StudentAttendance on StudentAttendance.AttendanceId=ClassAttendance.Id\r\n", con2);
            SqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read()) 
            {
                comboBox2.Items.Add((reader2.GetSqlDateTime(0)).ToString());
            }
            reader2.Close();

            cmd2.ExecuteNonQuery();



        }
        private void load_student_attendance()
        {

            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand($"  select CONCAT(FirstName,LastName)as NAME,RegistrationNumber,Lookup.Name as STATUS,AttendanceDate\r\nfrom ClassAttendance\r\njoin StudentAttendance\r\non StudentAttendance.AttendanceId=ClassAttendance.Id\r\njoin Student \r\non StudentAttendance.StudentId=Student.Id\r\njoin Lookup\r\non LookupId=StudentAttendance.AttendanceStatus\r\nwhere ClassAttendance.AttendanceDate='{x}'", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.DarkBlue;


        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            x = comboBox2.Items[comboBox2.SelectedIndex].ToString();
            load_student_attendance();
        }
        private void ExportToPDF(DataGridView dgv, string name, string l, string marks)
        {
            try
            {
                Document document = new Document(PageSize.A4, 20, 20, 20, 20);
            PdfWriter.GetInstance(document, new FileStream(name + ".pdf", FileMode.CreateNew));
            document.Open();
            iTextSharp.text.Font headingFont = FontFactory.GetFont("Times New Roman", 18, iTextSharp.text.Font.BOLD);
                Paragraph heading = new Paragraph(name, headingFont);
                heading.Alignment = Element.ALIGN_CENTER;
                heading.SpacingBefore = 10f;
                heading.SpacingAfter = 10f;

                document.Add(heading);

                LineSeparator line = new LineSeparator();
                document.Add(line);

                iTextSharp.text.Font headingFont2 = FontFactory.GetFont("Times New Roman", 14, iTextSharp.text.Font.BOLD);
                Paragraph heading2 = new Paragraph(marks, headingFont2);
                heading2.Alignment = Element.ALIGN_LEFT;
                heading2.SpacingBefore = 10f;
                heading2.SpacingAfter = 10f;

                document.Add(heading2);



                iTextSharp.text.Font courseFont = FontFactory.GetFont("Times New Roman", 12);
                Paragraph course = new Paragraph(l, courseFont);

                course.Alignment = Element.ALIGN_CENTER;
                course.IndentationLeft = 55f;
                course.SpacingAfter = 20f;
                document.Add(course);

                LineSeparator line2 = new LineSeparator();
                document.Add(line2);



                PdfPTable table = new PdfPTable(dgv.Columns.Count);
                table.WidthPercentage = 100;
                foreach (DataGridViewColumn column in dgv.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    table.AddCell(cell);
                }

                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.Index == datagridView.Rows.Count)
                    {
                        continue;

                    }
                    else
                    {
                        try
                        {
                            foreach (DataGridViewCell cell in row.Cells)
                            {

                                if (cell.Value == null)
                                {
                                    continue;
                                    MessageBox.Show("Fill all the columns of table (status) it can not be null");
                                }
                                else
                                {
                                    PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString()));
                                table.AddCell(pdfCell);
                                }
                            }
                        }
                        catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }

                    }


                }
                document.Add(table);
                document.Close();
            }
            catch (Exception exp) { }//MessageBox.Show("Fill all the columns of table (status) it can not be null"); }
            
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (datagridView.DataSource != null && comboBox2.Text != String.Empty)
            {
                string dateString = comboBox2.Text;
                DateTime dateonly;
                DateTime.TryParse(dateString, out dateonly);

                string datestring2 = dateonly.ToString("yyyy-MM-dd");
                // DateTime date = DateTime.ParseExact(dateString, "dd-MM-yyyy", CultureInfo.InvariantCulture); string namex = "Total Result of a " + comboBox2.Text + " in " + LBLX.Text;
                string namx = " Student Attendance Report (" + datestring2 + ")";
                string linex = "Attendance Report of Students on" + comboBox2.Text ;
                string date2= "Attendance Date " +datestring2;
               
                ExportToPDF(datagridView, namx, linex, date2);
                MessageBox.Show("Report Generated");
            }
            else { MessageBox.Show("Select the record first to generate report"); }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
