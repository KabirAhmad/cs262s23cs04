﻿using Org.BouncyCastle.Crypto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace CRUDA
{
    public partial class Assessment_Component_Form : Form
    {
        List<string> NameRubric = new List<string>();
        List<int> RubericIDS = new List<int>();
        int RubericID;

        List<string> Assessment = new List<string>();
        List<int> AssesmentID = new List<int>();
        int AssessID;
        bool check_date = false;
        string dateC, dataU;

        bool check_update = false;
        int id; 
        public Assessment_Component_Form()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            markstxt.Text = String.Empty;
            nametxt.Text = String.Empty;
        }
        private void l1()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select  Details FROM Rubric", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                NameRubric.Add(reader.GetString(0));
            }
            reader.Close();

            cmd.ExecuteNonQuery();
            
        }

        private void l2()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select  Details FROM Rubric", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                NameRubric.Add(reader.GetString(0));
            }
            reader.Close();

            cmd.ExecuteNonQuery();
            con.Close();
        }
        private void l3()
        {
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Select  id FROM Rubric", con2);
            SqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                RubericIDS.Add(Convert.ToInt16(reader2.GetInt32(0)));
            }
            reader2.Close();

            cmd2.ExecuteNonQuery();
            con2.Close();
        }
        private void load_combobox_rubric_data() 
        {

            l1();
            l2();
            l3();
            cmbxRuber.DataSource = NameRubric;

        }
        private void load_combobox_assessment_data()
        {
            load1();
            load2();
            comboBox1.DataSource = Assessment;



        }
        private void load1()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select  Title FROM Assessment", con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Assessment.Add(reader.GetString(0));
            }
            reader.Close();

            cmd.ExecuteNonQuery();
        }
        private void load2()
        {
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Select  id FROM Assessment", con2);
            SqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                AssesmentID.Add(Convert.ToInt16(reader2.GetInt32(0)));
            }
            reader2.Close();

            cmd2.ExecuteNonQuery();
            
        }
        private void Assessment_Component_Form_Load(object sender, EventArgs e)
        {

            load_combobox_assessment_data();
            load_combobox_rubric_data();
            set_date();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            AssessID= AssesmentID[comboBox1.SelectedIndex];
        }

        private void cmbxRuber_SelectedIndexChanged(object sender, EventArgs e)
        {
            RubericID = RubericIDS[cmbxRuber.SelectedIndex];
        }
        private void set_date()
        {
            dateTimePicker1.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker1.MaxDate = new DateTime(2023, 12, 31);
            dateTimePicker2.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker2.MaxDate = new DateTime(2023, 12, 31);
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            set_date_on_Click();
            if (check_date == false) 
            {
                if (markstxt.Text != String.Empty && nametxt.Text!=String.Empty)
                {
                    Random r = new Random();
                    int x = r.Next(0, 20);
                    var con = Configuration.getInstance().getConnection();
                    
                    SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@name,@RId,@marks,@dateC,@dateU,@AssID)", con);
                    cmd.Parameters.AddWithValue("@marks", markstxt.Text);
                    cmd.Parameters.AddWithValue("@name", nametxt.Text);
                    cmd.Parameters.AddWithValue("@AssID", AssessID.ToString());
                    cmd.Parameters.AddWithValue("@RId", RubericID.ToString());
                    cmd.Parameters.AddWithValue("@dateC", dateC);
                    cmd.Parameters.AddWithValue("@dateU", dataU);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    con.Close();
                    con.Open();
                    markstxt.Text = String.Empty;
                    nametxt.Text = String.Empty;
                }
                else { MessageBox.Show("Fill the data First"); }

            }
            else  
            {

                var con2 = Configuration.getInstance().getConnection();
                con2.Open();
                SqlCommand cmd2 = new SqlCommand("Update AssessmentComponent Set Name= @name,RubricId=@RId,TotalMarks=@marks,DateCreated=@dateC,DateUpdated=@dateU,AssessmentId=@AssID)where Id=@ID", con2);
                cmd2.Parameters.AddWithValue("@marks", markstxt.Text);
                cmd2.Parameters.AddWithValue("@name", nametxt.Text);
                cmd2.Parameters.AddWithValue("@AssID", AssessID.ToString());
                cmd2.Parameters.AddWithValue("@RId", RubericID.ToString());
                cmd2.Parameters.AddWithValue("@dateC", dateC);
                cmd2.Parameters.AddWithValue("@dateU", dataU);
                cmd2.Parameters.AddWithValue("@ID", id);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                con2.Close();
                markstxt.Text = String.Empty;
                nametxt.Text = String.Empty;
   

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblContactNumberSignal_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Lblontact_Click(object sender, EventArgs e)
        {

        }

        private void nametxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void LastNamelbl_Click(object sender, EventArgs e)
        {

        }

        private void lblSignUp_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblEmailAddressSignal_Click(object sender, EventArgs e)
        {

        }

        private void lblFirstName_Click(object sender, EventArgs e)
        {

        }

        private void lblFirstNameSingal_Click(object sender, EventArgs e)
        {

        }

        private void markstxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbllastNameSignal_Click(object sender, EventArgs e)
        {

        }

        private void Registerationlbl_Click(object sender, EventArgs e)
        {

        }

        private void lblResgSignal_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gbx_Enter(object sender, EventArgs e)
        {

        }

        private void lblRecordSignal_Click(object sender, EventArgs e)
        {

        }

        private void set_date_on_Click() 
        {
            DateTime selectedDateTime1 = dateTimePicker1.Value;
             dateC = selectedDateTime1.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime selectedDateTime2 = dateTimePicker2.Value;
             dataU = selectedDateTime2.ToString("yyyy-MM-dd HH:mm:ss");

            string dateFormat = "yyyy-MM-dd";
            bool validDate1 = DateTime.TryParseExact(dateC, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out selectedDateTime1);
            bool validDate2 = DateTime.TryParseExact(dataU, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out selectedDateTime2);

            if (validDate1 && validDate2)
            {
                if (selectedDateTime2 > selectedDateTime1 || selectedDateTime2 == selectedDateTime1)
                {
                    check_date = true;
                }
            }

        }
    }
}
