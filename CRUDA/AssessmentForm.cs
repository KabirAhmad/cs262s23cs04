﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using iTextSharp.tool.xml.html.head;
using System.Data.SqlTypes;
using iTextSharp.text.pdf;
using System.IO;

namespace CRUDA
{
    public partial class AssessmentForm : Form
    {
        bool check_update;
        int id, marks, weightage;
        string title;        
        
        public AssessmentForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Titletxtbx.Text = String.Empty;
            markstxtbx.Text = String.Empty;
            Weightagetxtbx.Text = String.Empty;
            
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            DateTime selectedDateTime = dateTimePicker1.Value;
            string sqlDateTime = selectedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            if (check_update == false)
            {


                var con = Configuration.getInstance().getConnection();
                con.Open();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values (@title,@date,@Marks,@Weightage)", con);
                cmd.Parameters.AddWithValue("@title", (Titletxtbx.Text));
                cmd.Parameters.AddWithValue("@date", sqlDateTime);
                cmd.Parameters.AddWithValue("@Marks", markstxtbx.Text);
                cmd.Parameters.AddWithValue("@Weightage", Weightagetxtbx.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                con.Close();
                Titletxtbx.Text = String.Empty;
                markstxtbx.Text = String.Empty;
                Weightagetxtbx.Text = String.Empty;




            }
            else if (check_update == true)
            {
                var con2 = Configuration.getInstance().getConnection();
                con2.Open();
                SqlCommand cmd2 = new SqlCommand("Update Assessment Set Title = @title, DateCreated = @date, TotalMarks = @Marks, TotalWeightage = @Weightage  WHERE Id = @ID", con2);
                cmd2.Parameters.AddWithValue("@title", (Titletxtbx.Text));
                cmd2.Parameters.AddWithValue("@date", sqlDateTime);
                cmd2.Parameters.AddWithValue("@Marks", markstxtbx.Text);
                cmd2.Parameters.AddWithValue("@Weightage", Weightagetxtbx.Text);
                cmd2.Parameters.AddWithValue("@ID", id);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                con2.Close();
                check_update = false;
                Titletxtbx.Text = String.Empty;
                markstxtbx.Text = String.Empty;
                Weightagetxtbx.Text = String.Empty;
            }

            Form ass= new Assessment_Component_Form();
            ass.Show();
            this.Close();
        }
        private void view()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from Assessment", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            con2.Close();


        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[2].Value.ToString());
                title = datagridView.Rows[e.RowIndex].Cells[3].Value.ToString();
                marks = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[5].Value.ToString());
                weightage = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[6].Value.ToString());

            }
            catch (Exception exp) { }
        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    Titletxtbx.Text = title;
                    markstxtbx.Text = marks.ToString();
                    Weightagetxtbx.Text = weightage.ToString();
                    check_update = true;


                }
                else if (index == 1)
                {
                    var con = Configuration.getInstance().getConnection();
                    con.Open();
                    SqlCommand cmd = new SqlCommand("Delete FROM Assessment  where Id= @id", con);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                    con.Close();    

                    MessageBox.Show("Deleted Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
            }
        }
        private void ExportToPDF(DataGridView dgv)
        {
            try
            {
                Document document = new Document(PageSize.A4, 20, 20, 20, 20);
                PdfWriter.GetInstance(document, new FileStream("TotaL Assessments.pdf", FileMode.Create));
                document.AddHeader("Header", "Report of  Assesment list");
                document.AddHeader("Date", dateTimePicker1.Text);
                document.Open();
                // Create a table with the same number of columns as the DataGridView
                PdfPTable table = new PdfPTable(dgv.Columns.Count);
                // Add the column headers from the DataGridView to the table
                foreach (DataGridViewColumn column in dgv.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    table.AddCell(cell);
                }

                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.Index == datagridView.Rows.Count - 1)
                    {
                        continue;

                    }
                    else
                    {
                        try
                        {
                            foreach (DataGridViewCell cell in row.Cells)
                            {

                                if (cell.Value == null)
                                {
                                    MessageBox.Show("Fill all the columns of table (status) it can not be null");
                                }
                                else
                                {
                                    PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString()));
                                    table.AddCell(pdfCell);
                                }
                            }
                        }
                        catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }

                    }


                }
                document.Add(table);
                document.Close();
            }
            catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }
            // Close the document
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExportToPDF(datagridView);
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblSignUp_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblFirstName_Click(object sender, EventArgs e)
        {

        }

        private void Weightagetxtbx_TextChanged(object sender, EventArgs e)
        {

        }

        private void markstxtbx_TextChanged(object sender, EventArgs e)
        {

        }

        private void Titletxtbx_TextChanged(object sender, EventArgs e)
        {

        }

        private void gbx_Enter(object sender, EventArgs e)
        {

        }

        private void lblRecordSignal_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            view();
        }

        private void AssessmentForm_Load(object sender, EventArgs e)
        {

            dateTimePicker1.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker1.MaxDate = new DateTime(2023, 12, 31);
            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Update";
            Update.Text = "Update";
            Update.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn Delete = new DataGridViewButtonColumn();
            Delete.HeaderText = "Delete";
            Delete.Text = "Delete";
            Delete.UseColumnTextForButtonValue = true;
            datagridView.Columns.Add(Update);
            datagridView.Columns.Add(Delete);
            view();
            bool check_update = false;
        }
    }
}
