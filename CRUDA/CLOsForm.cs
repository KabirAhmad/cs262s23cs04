﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.tool.xml.html.head;

namespace CRUDA
{
    public partial class CLOsForm : Form
    {
        bool check_update = false;
        string name;
        int id;
        public CLOsForm()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            nametxtbx.Text = String.Empty;
        }
        private void view()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from Clo", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            con2.Close();


        }
        private void btnCreateAccount_Click(object sender, EventArgs e)
        {

            DateTime selectedDateTime1 = dateTimePicker1C.Value;
            string dateC = selectedDateTime1.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime selectedDateTime2 = dateTimePickerU.Value;
            string dataU = selectedDateTime2.ToString("yyyy-MM-dd HH:mm:ss");
            bool check_date = false;
            string dateFormat = "yyyy-MM-dd";
            bool validDate1 = DateTime.TryParseExact(dateC, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out selectedDateTime1);
            bool validDate2 = DateTime.TryParseExact(dataU, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out selectedDateTime2);

            if (validDate1 && validDate2)
            {
                if (selectedDateTime2 > selectedDateTime1 || selectedDateTime2 == selectedDateTime1)
                {
                    check_date = true;
                }
            }

            if (check_update == false)
            {

                var con = Configuration.getInstance().getConnection();
                con.Open();
                SqlCommand cmd = new SqlCommand("Insert into Clo values (@title,@dateC,@dateUI)", con);
                cmd.Parameters.AddWithValue("@title", (nametxtbx.Text));
                cmd.Parameters.AddWithValue("@dateC", dateC);
                cmd.Parameters.AddWithValue("@dateUI", dateC);
                cmd.ExecuteNonQuery();
                MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                con.Close();
                nametxtbx.Text = String.Empty;





            }
            else 
            if (check_update == true)
            {
              //  if (check_date == true)
                //{
                    var con2 = Configuration.getInstance().getConnection();
                    con2.Open();
                    SqlCommand cmd2 = new SqlCommand("Update Clo Set Name=@title,DateCreated=@dateC,DateUpdated=@dateU where Id=@ID", con2);
                    cmd2.Parameters.AddWithValue("@title", (nametxtbx.Text));
                    cmd2.Parameters.AddWithValue("@dateC", dateC);
                    cmd2.Parameters.AddWithValue("@dateU", dataU);
                    cmd2.Parameters.AddWithValue("@ID", id);
                    cmd2.ExecuteNonQuery();
                    MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    con2.Close();
                    check_update = false;
                    nametxtbx.Text = String.Empty;
              //  }
               // else
                //{
                    MessageBox.Show("Updated date must be greater than or equal to created date");
                //}
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            view();
        }
        private void ExportToPDF(DataGridView dgv)
        {
            try
            {
                Document document = new Document(PageSize.A4, 20, 20, 20, 20);
                PdfWriter.GetInstance(document, new FileStream("TotaL CLO's.pdf", FileMode.Create));
                document.AddHeader("Header", "Report of  CLo's list");
                document.AddHeader("Date", DateTime.Now.ToString());
                document.Open();
                // Create a table with the same number of columns as the DataGridView
                PdfPTable table = new PdfPTable(dgv.Columns.Count);
                // Add the column headers from the DataGridView to the table
                foreach (DataGridViewColumn column in dgv.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    table.AddCell(cell);
                }

                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.Index == datagridView.Rows.Count - 1)
                    {
                        continue;

                    }
                    else
                    {
                        try
                        {
                            foreach (DataGridViewCell cell in row.Cells)
                            {

                                if (cell.Value == null)
                                {
                                    MessageBox.Show("Fill all the columns of table (status) it can not be null");
                                }
                                else
                                {
                                    PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString()));
                                    table.AddCell(pdfCell);
                                }
                            }
                        }
                        catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }

                    }


                }
                document.Add(table);
                document.Close();
            }
            catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }
            // Close the document
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExportToPDF(datagridView);
        }

        private void CLOsForm_Load(object sender, EventArgs e)
        {
            dateTimePicker1C.MinDate = new DateTime(2023, 1, 1);
            dateTimePicker1C.MaxDate = new DateTime(2023, 12, 31);
            dateTimePickerU.MinDate = new DateTime(2023, 1, 1);
            dateTimePickerU.MaxDate = new DateTime(2023, 12, 31);
            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Update";
            Update.Text = "Update";
            Update.UseColumnTextForButtonValue = true;

            datagridView.Columns.Add(Update);

            view();
            bool check_update = false;
        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    nametxtbx.Text = name;
                    
                    check_update = true;


                }
   
            }
        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[2].Value.ToString());
                name = datagridView.Rows[e.RowIndex].Cells[3].Value.ToString();


            }
            catch (Exception exp) { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gbx_Enter(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1C_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblFirstName_Click(object sender, EventArgs e)
        {

        }

        private void nametxtbx_TextChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dateTimePickerU_ValueChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblRecordSignal_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblSignUp_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
