﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDA
{
    public partial class StudentRC : UserControl
    {
        String Name;
        int ID;

        public StudentRC()
        {
            InitializeComponent();
        }

        private void StudentRC_Load(object sender, EventArgs e)
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select *  from Assessment", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;

            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Evaluate";
            Update.Text = "Evaluate";
            Update.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn Delete = new DataGridViewButtonColumn();
            Delete.HeaderText = "Result";
            Delete.Text = "Result";
            Delete.UseColumnTextForButtonValue = true;
            datagridView.Columns.Add(Update);
            datagridView.Columns.Add(Delete);
        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    EvaluationC newUserControl = new EvaluationC( Name, ID);
                    newUserControl.Dock = DockStyle.Fill;
                    this.Parent.Controls.Add(newUserControl);
                    newUserControl.BringToFront();
                    this.Hide();
                

                }
                else if (index == 1)
                {
                    ResultAC newUserControl = new ResultAC(ID, Name);
                    newUserControl.Dock = DockStyle.Fill;
                    this.Parent.Controls.Add(newUserControl);
                    newUserControl.BringToFront();
                    this.Hide();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                Name = datagridView.Rows[e.RowIndex].Cells[3].Value.ToString();
                ID = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[2].Value.ToString());



            }
            catch (Exception exp) { }
        }
    }
}
