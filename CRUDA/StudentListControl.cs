﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CRUDA
{
    public partial class StudentListControl : UserControl
    {
        string first, last, registeration, email, contact;
        int id, status;
        public StudentListControl()
        {
            InitializeComponent();
        }
        private void DataBind()
        {


            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Update";
            Update.Text = "Update";
            Update.UseColumnTextForButtonValue = true;

            datagridView.Columns.Add(Update);

        }
        private void load_data_table()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            DataBind();
            totalcount.ForeColor = Color.Navy;
        }

        private void load_1()
        {
            var con4 = Configuration.getInstance().getConnection();
              //con4.Open();
            SqlCommand command4 = new SqlCommand("Select count(*) from Student where status=6", con4);
            int count2 = (int)command4.ExecuteScalar();
            Inactivecount.Text = count2.ToString();
            con4.Close();
            con4.Open();
        }
        private void load_2()
        {
            var con3 = Configuration.getInstance().getConnection();
            //con3.Open();
            SqlCommand command3 = new SqlCommand("Select count(*) from Student", con3);
            int count3 = (int)command3.ExecuteScalar();
            totalcount.Text = count3.ToString();
           // con3.Close();
        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    Student newUserControl = new Student(id, first, last, registeration, email, contact, status, true);
                    newUserControl.Dock = DockStyle.Fill;
                    this.Parent.Controls.Add(newUserControl);
                    newUserControl.BringToFront();
                    this.Hide();



                }

            }

        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[1].Value.ToString());
                first = datagridView.Rows[e.RowIndex].Cells[2].Value.ToString();
                last = datagridView.Rows[e.RowIndex].Cells[3].Value.ToString();
                contact = datagridView.Rows[e.RowIndex].Cells[4].Value.ToString();
                email = datagridView.Rows[e.RowIndex].Cells[5].Value.ToString();
                registeration = datagridView.Rows[e.RowIndex].Cells[6].Value.ToString();
                status = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[7].Value.ToString());
           
            }
            catch (Exception exp) { }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void load_3()
        {
            var con4 = Configuration.getInstance().getConnection();
            //con4.Open();
            SqlCommand command = new SqlCommand("Select count(*) from Student where status=5", con4);
            int count = (int)command.ExecuteScalar();
            activecount.Text = count.ToString();
           // con4.Close();
            //con4.Open();
        }
        private void StudentControl_Load(object sender, EventArgs e)
        {
            load_data_table();
            load_1();
            load_2();
            load_3();

        }
        private void ExportToPDF(DataGridView dgv)
        {

            Document document = new Document(PageSize.A4, 20, 20, 20, 20);
            document.Open();
            PdfWriter.GetInstance(document, new FileStream("Student Reports.pdf", FileMode.Create));
            document.AddHeader("Header", "Report of Student list");

            // Add the paragraph to the document
            
            document.AddHeader("Date", DateTime.Now.ToString());

            Chunk mainHeading = new Chunk("Main Heading", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 18));

            // Create a Paragraph object and add the main heading to it
            Paragraph paragraph = new Paragraph();
            paragraph.Add(mainHeading);
            paragraph.SpacingAfter = 10f;

            
            // Create a table with the same number of columns as the DataGridView
            PdfPTable table = new PdfPTable(dgv.Columns.Count);


            // Set the width of each column in the table
           
            // Add the column headers from the DataGridView to the table
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                table.AddCell(cell);
            }

            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Index == datagridView.Rows.Count - 1)
                {
                    continue;

                }
                else
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString()));
                        table.AddCell(pdfCell);
                    }

                }


            }
            document.Open();
            float[] columnWidths = new float[dgv.Columns.Count];
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                columnWidths[i] = 100f;
            }
            table.SetWidths(columnWidths);
            document.Add(table);
            document.Close();
            MessageBox.Show("done");
            // Close the document
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportToPDF(datagridView);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
