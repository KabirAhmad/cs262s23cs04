﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf.draw;
using System.ComponentModel.Design.Serialization;

namespace CRUDA
{
    public partial class ReportsC : UserControl
    {
        string name;
        string line;
        public ReportsC()
        {
            InitializeComponent();
        }
        private void load1()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand($"  select \r\n  st.StudentId,s.FirstName as Name, s.RegistrationNumber as RegNo,Assessment.Title as Assessment,\r\n  st.AssessmentComponentId as ACID ,\r\n  ac.TotalMarks as Marks,\r\n  rl.MeasurementLevel,\r\n  max(rl2.MeasurementLevel)  as MaxLevel ,\r\n  Cast (CAST(rl.MeasurementLevel AS FLOAT)/ max( CAST(rl2.MeasurementLevel AS FLOAT) )* ac.TotalMarks as float) as OMarks\r\n\r\n  from StudentResult as st\r\n  join Student as s\r\n  on st.StudentId=s.Id\r\n  join AssessmentComponent as ac\r\n  on ac.Id=st.AssessmentComponentId\r\n  join Rubric as r\r\n  on r.Id=ac.RubricId\r\n  join RubricLevel as rl\r\n  on rl.Id=st.RubricMeasurementId\r\n  join RubricLevel as rl2\r\n  on rl2.RubricId=r.Id\r\n  join Assessment\r\n  on Assessment.Id=ac.AssessmentId\r\n  group by st.StudentId,st.AssessmentComponentId,ac.TotalMarks,rl.MeasurementLevel,s.FirstName,s.RegistrationNumber,Assessment.Title", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            con2.Close();
            con2.Open();
        }
        private void load2()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand($"SELECT S.RegistrationNumber,  SUM(AC.TotalMarks)AS TotalMarks,Cast(SUM(((RL.MeasurementLevel / (RL.Maximum * 1.0)) * AC.TotalMarks))as int) AS ObtainedMarks\r\nFROM Student S\r\n JOIN StudentResult SR ON S.Id = SR.StudentId\r\n JOIN AssessmentComponent AC ON AC.Id = SR.AssessmentComponentId\r\n JOIN Assessment A ON A.Id = AC.AssessmentId\r\n JOIN Rubric R ON R.Id = AC.RubricId\r\n JOIN Clo C ON C.Id = R.CloId\r\n JOIN (SELECT Id, RubricId, MeasurementLevel, R.Maximum FROM RubricLevel, \r\n           (SELECT RubricId AS RID, MAX(RubricLevel.MeasurementLevel) AS Maximum \r\n            FROM RubricLevel GROUP BY RubricId) AS R \r\n           WHERE RubricLevel.RubricId = R.RID) AS RL\r\nON SR.RubricMeasurementId = RL.ID\r\nGROUP BY S.RegistrationNumber\r\n", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            con2.Close();
            con2.Open();
        }
        private void load3()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand($"    select \r\nConcat(s.FirstName,s.LastName) as StudentName, s.RegistrationNumber,Clo.Name,Assessment.Title,\r\nac.name as AComponent,\r\n  ac.TotalMarks as totalmarks,\r\n\r\n  Cast (CAST(rl.MeasurementLevel AS FLOAT)/ max( CAST(rl2.MeasurementLevel AS FLOAT) )* ac.TotalMarks as float) as Obtainedarks\r\n\r\n  from StudentResult as st\r\n  join Student as s\r\n  on st.StudentId=s.Id\r\n  join AssessmentComponent as ac\r\n  on ac.Id=st.AssessmentComponentId\r\n  join Rubric as r\r\n  on r.Id=ac.RubricId\r\n  join RubricLevel as rl\r\n  on rl.Id=st.RubricMeasurementId\r\n  join RubricLevel as rl2\r\n  on rl2.RubricId=r.Id\r\n  join Assessment\r\n  on Assessment.Id=ac.AssessmentId\r\n  join Clo\r\n  on r.CloId=Clo.Id\r\n\r\n  group by st.AssessmentComponentId,ac.TotalMarks,rl.MeasurementLevel,Concat(s.FirstName,s.LastName),s.RegistrationNumber,Clo.Name,ac.Name,Assessment.Title\r\n", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            con2.Close();
            con2.Open();
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void ExportToPDF(DataGridView dgv,string name, string l)
        {
            try
            {
                Document document = new Document(PageSize.A4, 20, 20, 20, 20);
                PdfWriter.GetInstance(document, new FileStream(name+".pdf", FileMode.Create));
                document.Open();
                iTextSharp.text.Font headingFont = FontFactory.GetFont("Times New Roman", 18, iTextSharp.text.Font.BOLD);
                Paragraph heading = new Paragraph(name, headingFont);
                heading.Alignment = Element.ALIGN_CENTER;
                heading.SpacingBefore = 10f;
                heading.SpacingAfter = 10f;

                document.Add(heading);

                LineSeparator line = new LineSeparator();
                document.Add(line);


                iTextSharp.text.Font courseFont = FontFactory.GetFont("Times New Roman", 12);
                Paragraph course = new Paragraph(l, courseFont);

                course.Alignment = Element.ALIGN_CENTER;
                course.IndentationLeft = 55f; 
                course.SpacingAfter = 20f;
                document.Add(course);

                LineSeparator line2 = new LineSeparator();
                document.Add(line2);



                PdfPTable table = new PdfPTable(dgv.Columns.Count);
                table.WidthPercentage = 100;
                foreach (DataGridViewColumn column in dgv.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    table.AddCell(cell);
                }

                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.Index == datagridView.Rows.Count )
                    {
                        continue;

                    }
                    else
                    {
                        try
                        {
                            foreach (DataGridViewCell cell in row.Cells)
                            {

                                //if (cell.Value == null)
                                //{
                                //    continue;
                                //    MessageBox.Show("Fill all the columns of table (status) it can not be null");
                                //}
                                //else
                                //{
                                    PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString()));
                                    table.AddCell(pdfCell);
                                //}
                            }
                        }
                        catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }

                    }


                }
                document.Add(table);
                document.Close();
            }
            catch (Exception exp) { MessageBox.Show("Fill all the columns of table (status) it can not be null"); }
            // Close the document
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text == "Assessment wise Result of Student") {

                load1();


            }
            else if (comboBox2.Text == "Clo Wise Result of Students(who attempts Assessments)") {

                load2();
            }
            else if (comboBox2.Text == "Clo Wise Marks in Assessment Components of Students")
            {

                load3();
            }
            else if (comboBox2.Text == "Total Result of a Specific Student in Assesment")
            {

                MessageBox.Show("TO check this report sample on Generate Report");
            }

        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text == "Assessment wise Result of Student")
            {
                name=comboBox2.Text;
                line = "Report of Students per Assessment Wise shows the marks of each Assessment Components";
                ExportToPDF(datagridView, name,line);
                MessageBox.Show("Report Generated");

            }
            else if (comboBox2.Text == "Clo Wise Result of Students(who attempts Assessments)")
            {
                name = "CLO Wise Result of Students";
                line = "Report of Clo Wise result of Students Who attempts Assessment";
                ExportToPDF(datagridView, name, line);
                MessageBox.Show("Report Generated");
            }
            else if (comboBox2.Text == "Clo Wise Marks in Assessment Components of Students")
            {
                name = "CLO Wise Result of Assessment Componenets ";
                line = "Report of Clo Wise result of Students According to the Assessment Components they have attempted yet";
                ExportToPDF(datagridView, name, line);
                MessageBox.Show("Report Generated");
            }
            else if (comboBox2.Text == "Total Result of a Specific Student in Assesment")
            {
                StudentRC newUserControl = new StudentRC();
                MessageBox.Show("Select the Assessment and click on result to generate Report");
                newUserControl.Dock = DockStyle.Fill;
                this.Parent.Controls.Add(newUserControl);
                newUserControl.BringToFront();
                this.Hide();

            }
            else if (comboBox2.Text == "Attendance Report")
            {
                ViewAttendanceC newUserControl = new ViewAttendanceC();
                MessageBox.Show("Select the date and click on result to generate Report");
                newUserControl.Dock = DockStyle.Fill;
                this.Parent.Controls.Add(newUserControl);
                newUserControl.BringToFront();
                this.Hide();

            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
