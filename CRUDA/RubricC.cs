﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace CRUDA
{
    public partial class RubricC : UserControl
    {
        bool check_update = false;
        int id;
        string detail;
        int CloSID;
        public RubricC()
        {
            InitializeComponent();
        }
        private void view()
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from Rubric", con2);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            datagridView.DataSource = null;
            datagridView.DataSource = dt;
            datagridView.DefaultCellStyle.ForeColor = Color.Black;
            con2.Close();


        }
        private string check_q(string x)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand($"        IF(select max(1) from Rubric where Details='{x}' )>0 BEGIN SELECT '1' END ELSE BEGIN SELECT '2' END", con);
            string z = "";
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                z = (reader.GetString(0));
            }
            reader.Close();

            // X=cmd.ExecuteReader().GetString(0);
            cmd.ExecuteNonQuery();
            return z;


        }
        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            string z = check_q(detailstxt.Text);

            
                if (check_update == false && z != "1")
                {
                    if (detailstxt.Text != String.Empty)
                    {
                        Random r = new Random();
                        int x = r.Next(0, 200);
                        var con = Configuration.getInstance().getConnection();
                        //con.Open();
                        SqlCommand cmd = new SqlCommand($"Insert into Rubric values ({x},'{detailstxt.Text}',{CloSID})", con);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show(" Added  SuccessFully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //.con.Close();
                        detailstxt.Text = String.Empty;
                    }
                    else { MessageBox.Show("Fill the data First"); }


                }
                else
                if (check_update == true)
                {

                    var con2 = Configuration.getInstance().getConnection();
                    // con2.Open();
                    SqlCommand cmd2 = new SqlCommand("Update Rubric Set Details=@Detail,CloId=@CLoID where Id=@ID", con2);
                    cmd2.Parameters.AddWithValue("@Detail", detailstxt.Text);
                    cmd2.Parameters.AddWithValue("@CloID", CloSID);
                    cmd2.Parameters.AddWithValue("@ID", id);
                    cmd2.ExecuteNonQuery();
                    MessageBox.Show("UPDATED Successfully", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //con2.Close();
                    check_update = false;
                    detailstxt.Text = String.Empty;

                }
                else if (z == "1") { MessageBox.Show("Already Exist"); }

            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            detailstxt.Text = String.Empty;
            comboBox1.Text = String.Empty;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            view();
        }

        private void datagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = datagridView.CurrentCell.ColumnIndex;
            {

                if (index == 0)
                {
                    detailstxt.Text = detail;


                    check_update = true;


                }

            }
        }
        private void load_combobox_assessment_data()
        {

            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            load1();
            load2();

        }
        private void load2()
        {
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Select  id FROM Clo", con2);
            SqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                comboBox2.Items.Add(Convert.ToInt16(reader2.GetInt32(0)));
            }
            reader2.Close();

            cmd2.ExecuteNonQuery();

        }
        private void load1()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select  Name FROM Clo", con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                comboBox1.Items.Add(reader.GetString(0));
            }
            reader.Close();

            cmd.ExecuteNonQuery();
            con.Close();
            con.Open();
        }
        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RubricC_Load(object sender, EventArgs e)
        {
            DataGridViewButtonColumn Update = new DataGridViewButtonColumn();
            Update.HeaderText = "Update";
            Update.Text = "Update";
            Update.UseColumnTextForButtonValue = true;

            datagridView.Columns.Add(Update);

            view();
            bool check_update = false;
            load_combobox_assessment_data();
            
        }

        private void datagridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {


            try
            {
                id = Convert.ToInt16(datagridView.Rows[e.RowIndex].Cells[1].Value.ToString());
                detail = datagridView.Rows[e.RowIndex].Cells[2].Value.ToString();


            }
            catch (Exception exp) { }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CloSID = Convert.ToInt32(comboBox2.Items[comboBox1.SelectedIndex].ToString());
        }

        private void label4_Click(object sender, EventArgs e)
        {
            RubricLevelC newUserControl = new RubricLevelC();
            newUserControl.Dock = DockStyle.Fill;
            this.Parent.Controls.Add(newUserControl);
            newUserControl.BringToFront();
            this.Hide();


        }
    }
}
