  select 
  st.StudentId,s.FirstName as StudentName, s.RegistrationNumber,
  st.AssessmentComponentId,
  ac.TotalMarks as totalmarks,
  rl.MeasurementLevel,
  max(rl2.MeasurementLevel)  as MaximumMeasuremnetLevel ,
  Cast (CAST(rl.MeasurementLevel AS FLOAT)/ max( CAST(rl2.MeasurementLevel AS FLOAT) )* ac.TotalMarks as float) as Obtainedarks

  from StudentResult as st
  join Student as s
  on st.StudentId=s.Id
  join AssessmentComponent as ac
  on ac.Id=st.AssessmentComponentId
  join Rubric as r
  on r.Id=ac.RubricId
  join RubricLevel as rl
  on rl.Id=st.RubricMeasurementId
  join RubricLevel as rl2
  on rl2.RubricId=r.Id
  join Assessment
  on Assessment.Id=ac.AssessmentId
  where Assessment.Title='ProblemSet1'
  and s.RegistrationNumber='2021-CS-4'
  group by st.StudentId,st.AssessmentComponentId,ac.TotalMarks,rl.MeasurementLevel,s.FirstName,s.RegistrationNumber

