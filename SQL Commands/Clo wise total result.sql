SELECT S.RegistrationNumber,  SUM(AC.TotalMarks)AS TotalMarks,SUM(((RL.MeasurementLevel / (RL.Maximum * 1.0)) * AC.TotalMarks)) AS ObtainedMarks
FROM Student S
 JOIN StudentResult SR ON S.Id = SR.StudentId
 JOIN AssessmentComponent AC ON AC.Id = SR.AssessmentComponentId
 JOIN Assessment A ON A.Id = AC.AssessmentId
 JOIN Rubric R ON R.Id = AC.RubricId
 JOIN Clo C ON C.Id = R.CloId
 JOIN (SELECT Id, RubricId, MeasurementLevel, R.Maximum FROM RubricLevel, 
           (SELECT RubricId AS RID, MAX(RubricLevel.MeasurementLevel) AS Maximum 
            FROM RubricLevel GROUP BY RubricId) AS R 
           WHERE RubricLevel.RubricId = R.RID) AS RL
ON SR.RubricMeasurementId = RL.ID
GROUP BY S.RegistrationNumber
