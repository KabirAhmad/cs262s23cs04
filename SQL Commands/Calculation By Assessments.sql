  select 
s.FirstName as StudentName, s.RegistrationNumber,Assessment.Title,
ac.Name as AssComp,
  ac.TotalMarks as totalmarks,
  rl.MeasurementLevel as O_Level,
  max(rl2.MeasurementLevel)  as MaxLevel ,
  Cast (CAST(rl.MeasurementLevel AS FLOAT)/ max( CAST(rl2.MeasurementLevel AS FLOAT) )* ac.TotalMarks as float) as Obtainedarks

  from StudentResult as st
  join Student as s
  on st.StudentId=s.Id
  join AssessmentComponent as ac
  on ac.Id=st.AssessmentComponentId
  join Rubric as r
  on r.Id=ac.RubricId
  join RubricLevel as rl
  on rl.Id=st.RubricMeasurementId
  join RubricLevel as rl2
  on rl2.RubricId=r.Id
  join Assessment
  on Assessment.Id=ac.AssessmentId
  where RegistrationNumber='2021-CS-4' and Assessment.Title='ProblemSet1'
  group by ac.Name,ac.TotalMarks,rl.MeasurementLevel,s.FirstName,s.RegistrationNumber,Assessment.Title